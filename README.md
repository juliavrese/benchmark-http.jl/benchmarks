# Benchmark: HTTP vs GinTonic vs Flask

The following repository contains a benchmark analysis between same web services written in 3 different framework and languages. It is a simple single router web services. Where some user send a json post request such as `{"from": "Saru", "message": "Saru to Discovery. Over!"}` then the webservice read the from part and make new response and send it back to the requester.

## Methodology:
The test has has been run in Google Cloud Run. Each app is deployed in a 1 processor instance with docker in the Cloud Run. Then for concurrent request has been sent to server multiple times to measure the response time on the client side. So the client App will send send 50, 100 ... 1200 concurrent calls multiple times and then log into text files. The client is written in Go but very basic. None of the apps are production ready it is just for test purpose. No middle ware or error handling has been considered. Later the test files were accumulated to measure the median call for each concurrency group. Though Go and Julia based web server is expected to be better but when testing Flask-Gunicorn based server has the same performance like other 2 servers which is kind of surprise. The data is available in data directory can be tested.

![benchmark](./img/benchmark.png)